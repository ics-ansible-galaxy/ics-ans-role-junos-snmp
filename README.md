# ics-ans-role-junos-snmp

Ansible role to install junos-snmp.

## Role Variables

```yaml
junos_snmp_description: test switch
junos_snmp_location: communication_room
junos_snmp_contact: network_team
junos_snmp_community: test_group
junos_snmp_clients: 192.168.1.0/24
junos_snmp_hostname: ics-ans-role-junos-snmp-default
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-snmp
```

## License

BSD 2-clause
