import os
import testinfra.utils.ansible_runner


host = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    assert os.system("zcat /config/juniper.conf.gz | grep 192.168.1.0")
